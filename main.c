#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <assert.h>
#include <math.h>

extern int hello_world(void);
extern double mc_pi_asm(int);

double mc_pi_c(int iter);

const int iters = 1000000;

int main(void) {
	// assemly shared library test
	int val = hello_world();
	printf("main: %i\n\n", val);

	double deviation, pi_est;
	clock_t t;

	// asm montecarlo pi est.
	printf("running monte carlo pi estimation %dx\n\n", iters);

	t = clock();

	pi_est = mc_pi_asm(iters);

	t = clock() - t;

	deviation = fabs(pi_est - M_PI);
	printf("ASM[%fs]: PI estimated to: %f, deviation: %f\n",
		((float)t) / CLOCKS_PER_SEC, pi_est, deviation);

	// c montecarlo pi est.
	t = clock();

	pi_est = mc_pi_c(iters);

	t = clock() - t;

	deviation = fabs(pi_est - M_PI);
	printf("  C[%fs]: PI estimated to: %f, deviation: %f\n",
		((float)t) / CLOCKS_PER_SEC, pi_est, deviation);

	return 0;
}

double mc_pi_c(int iter) {
	double rand_op = RAND_MAX / 2;
	double rand_x, rand_y;
	int circle_points = 0, square_points = iter;

	srand(time(NULL));

	for (int i = 0; i < iter; i++) {
		rand_x = (double) rand() / RAND_MAX;
		rand_y = (double) rand() / RAND_MAX;

		if (rand_x * rand_x + rand_y * rand_y <= 1) {
			circle_points++;
		}
	}

	return 4. * circle_points / square_points;
}
