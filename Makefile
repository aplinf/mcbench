BUILD_DIR := ./build

all: run

run: $(BUILD_DIR)/main
	@LD_LIBRARY_PATH=build/ ./$<

$(BUILD_DIR)/main: $(BUILD_DIR)/main.o $(BUILD_DIR)/libmcpi.so
	cc $< -o $@ -Lbuild -lmcpi

$(BUILD_DIR)/main.o: main.c
	mkdir -p $(BUILD_DIR)
	cc -g -c $< -o $@

$(BUILD_DIR)/libmcpi.so: $(BUILD_DIR)/libmcpi.o
	ld -shared $< -o $@

$(BUILD_DIR)/libmcpi.o: libmcpi.yasm
	mkdir -p $(BUILD_DIR)
	yasm -f elf64 $< -o $@



clean:
	rm -r $(BUILD_DIR)
